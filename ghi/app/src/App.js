import Nav from './Nav';
import AttendeesList from './attendeesList';
import LocationForm from './locationForm';
import ConferenceForm from './conferenceForm';
import AttendConferenceForm from './attendConference';
import PresentationForm from './presentationForm';
import MainPage from './MainPage';
import {BrowserRouter, Routes, Route} from "react-router-dom";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
          <Routes>
            <Route path="MainPage" index element={<MainPage/>}/>
            <Route path="conferences/new" element={<ConferenceForm />} />
            <Route path="attendees/new" element={<AttendConferenceForm />} />
            <Route path="locations/new" element={<LocationForm />} />
            <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
            <Route path="presentation/new" element={<PresentationForm/>}/>
          </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
