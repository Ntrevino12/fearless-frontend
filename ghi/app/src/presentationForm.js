import React, {useEffect, useState} from 'react';

function PresentationForm(){

    const [conferences, setConferences] = useState([]);
    const [email, setEmail] = useState('');
    const [conference, setConference] = useState('');
    const [name, setName] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');

    const fetchData = async () => {
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const response = await fetch(conferenceUrl);
        if (response.ok){
            const data = await response.json()
            setConferences(data.conferences)
        }
    }
    useEffect(()=>{
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.conference = conference;
        data.presenter_name = name
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        data.presenter_email = email;

        const presentationUrl = `http://localhost:8000${conference}presentations/`
        const fetchOptions ={
            method:'post',
            body: JSON.stringify(data),
            headers: {
                'content-Type': 'application/json',
            },
        };
        const presentationResponse = await fetch(presentationUrl, fetchOptions)
        if(presentationResponse.ok){
            const newPresentation = await presentationResponse.json()
            console.log(newPresentation)
            setConference('');
            setName('');
            setCompanyName('');
            setTitle('');
            setSynopsis('');
            setEmail('');
        }
    }
    const handleChangeConference = (event) => {
        setConference(event.target.value);
    }
    const handleChangeName = (event) => {
        setName(event.target.value);
    }
    const handleChangeCompanyName = (event) => {
        setCompanyName(event.target.value)
    }
    const handleChangeTitle = (event) => {
        setTitle(event.target.value)
    }
    const handleChangeSynopsis = (event) => {
        setSynopsis(event.target.value)
    }
    const handleChangeEmail = (event) => {
        setEmail(event.target.value)
    }







    return(
        <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handleChangeName} value={name} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChangeEmail} value={email} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChangeCompanyName} value={companyName} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChangeTitle} value={title} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleChangeSynopsis} value={synopsis} className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleChangeConference} value={conference} required name="conference" id="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                    return (
                        <option key={conference.href} value={conference.href}>{conference.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    )
}

export default PresentationForm
